package uz.pdp.backend.utils;

import lombok.Getter;

@Getter

public enum MessageKey {
    BACK("back", "Orqaga"),
    INFORMATION_ERROR("information.error", """
            Siz kiritgan ma'lumotlar noto'g'ri!
            """.trim()),
    CHOOSE_LANGUAGE("choose.language", """
            Assalomu aleykum!
            Tilni tanlang:
            """.trim()),
    SHARE_PHONE_NUMBER("share.phone.number", """
            Telefon raqamingizni kiriting.
            """.trim()),
    SHARE_PHONE_NUMBER_BUTTON("share.phone.number.button", """
            Telefon raqamni ulashish
            """.trim()),
    MAIN_MENU("main.menu", """
            Asosiy menyu.
            """.trim()),
    SETTINGS_BUTTON("settings.button", """
            Sozlamalar
            """.trim()),
    SETTINGS("settings", """
            Sozlamalar menyusi.
            """.trim()),
    ORDER_BUTTON("order.button", """
            Buyurtma berish
            """.trim()),
    // properties yozish kerak
    SUBMIT_APPLICATION_BUTTON("submit.application.button", """
            Ariza yuborish
            """.trim()),
    // properties yozish kerak
    CHOOSE_APPLICATION_TYPE("choose.application.type", """
            Ariza turini tanlang.
            """.trim()),

    BUSINESS_APPLICATION_BUTTON("business.application.button", """
            Biznes
            """.trim()),
    // properties yozish kerak
    COURIER_APPLICATION_BUTTON("courier.application.button", """
            Kuryer
            """.trim()),
    BUSINESS_APPLICATION_ENTER_NAME("business.application.enter.name", """
            Biznes nomini kiriting.
            """.trim()),
    BUSINESS_APPLICATION_ENTER_TYPE("business.application.enter.type", """
            Biznes turini kiriting.
            """.trim()),
    BUSINESS_APPLICATION_ENTER_LOCATION("business.application.enter.location", """
            Biznes joylashgan joyni kiriting.
            """.trim()),
    FINISHED_BUSINESS_APPLICATION("finished.business.application", """
            Ariza muvaffaqiyatli yuborildi!
            """.trim()),
    // properties yozish kerak
    COURIER_APPLICATION_ENTER_BIRTH_DATE("courier.application.enter.birth.date", """
            Tu'gilgan kuningizni kiriting.
            Format = kun.oy.yil[dd.MM.yyyy].
            """.trim()),
    SET_LANGUAGE_BUTTON("set.language.button", """
            Tilni o'zgartirish
            """.trim()),
    SET_LANGUAGE("set.language", """
            Tilni o'zgartirish.
            Tilni tanlang:
            """.trim()),

    SHARE_LOCATION_BUTTON("share.location.button", """
            Joylashuvni yuborish
            """.trim()),

    // ------------------------------------------ADMIN MESSAGE KEY-------------------------------------------------,
    STARTED_ADMIN("started.admin", """
            Assalomu aleykum admin.
            %s!
            """.trim()),
    ADMIN_PANEL("admin.panel", """
            Admin panel.
            """.trim()),

    GET_ALL_APPLICATION_BUTTON("get.all.application.button", """
            Arizalarni ko'rish
            """.trim()),

    SETTINGS_ADMIN("settings.admin", """
            Sozlamalar menyusi.
            """.trim()),
    ADD_CONTACT_ADMIN("add.contact.admin", """
            Telefon raqamni qo'shish menyusi.
            """.trim()),
    CHANGE_CONTACT_BUTTON("change.contact.button", """
            Telefon raqamni o'zgartirish
            """.trim()),
    APPLICATIONS_ADMIN("applications.admin", """
            Arizalar ro'yxati:
            """.trim()),
    APPLICATIONS_TYPE_ADMIN("applications.type.admin", """
            Arizalar turi:
            """.trim()),
    WRONG_ROLE_ADMIN("wrong.role.admin", """
            Siz admin emassiz!
            """.trim()),
    OPEN_BUSINESS_APPLICATION("open.business.application", """
            Yuboruvchi:
                Ism Familiya : %s
                Foydalanuvchi nomi : @%s
                Telefon raqami : %s
            Biznesning Nomi : %s
            Biznesning Tipi : %s
            Ariza yuborilgan vaqt : %s
            """.trim()),
    OPEN_COURIER_APPLICATION("open.courier.application", """
            Yuboruvchi:
                Ism Familiya : %s
                Foydalanuvchi nomi : @%s
                Telefon raqami : %s
            Ariza yuborilgan vaqt : %s
            """.trim()),
    ACCEPT_BUSINESS_APPLICATION("accept.business.location", """
            Qabul qilish
            """.trim()),
    REJECT_BUSINESS_APPLICATION("reject.business.location", """
            Rad etish
            """.trim()),
    ACCEPT_COURIER_APPLICATION("accept.courier.application", """
            Qabul qilish
            """.trim()),
    REJECT_COURIER_APPLICATION("reject.courier.application", """
            Rad etish
            """.trim()),

    WRITE_COMMENT_ADMIN("write.comment.admin", """
            Rad etish sababini kiriting:
            """.trim()),
    ACCEPTED_APPLICATION_ADMIN("accepted.application.admin", """
            Admin sizning arizangizni ko'rib chiqdi va qabul qildi.
            """.trim()),
    REJECTED_APPLICATION_ADMIN("rejected.application.admin", """
            Admin sizning arizangizni rad etdi!\nRad etish sababi: \n 
            """.trim()),
    BUSINESS_TYPE_APPLICATION_BUTTON("business.type.application.button", """
            Biznes
            """.trim()),
    COURIER_TYPE_APPLICATION_BUTTON("courier.type.application.button", """
            Kuryer
            """.trim()),
    GET_REPORT_BUTTON("get.report.button", """
            Hisobotlar
            """.trim()),

    REPORTS_ADMIN("reports.admin", """
            Foydalanuvchilar soni : %s
            Kuryerlar soni : %s
            Bizneslar soni : %s
            """.trim()),
    SEARCH_USER_BUTTON("search.user.button", """
            Foydalanuvchini qidirish
            """.trim()),
    SEARCH_USER("search.user", """
            Foydalanuvchining telefon raqamini kiriting:
            """.trim()),
    SEARCHED_USER_INFO("searched.user.info", """
            Qidirilgan foydalanuvchi:
                Tili: %s
                Ism Familiya: %s
                Foydalanuvchi nomi: %s
                Telefon raqami: %s
                Tug'ilgan sanasi: %s
                Roli: %s
                Aktivligi: %s
            """.trim()),
    TO_BAN("to.ban", """
            Bloklash!
            """.trim()),
    USER_NOT_FOUND("user.not.found", """
            Foydalanuvchi topilmadi!
            """.trim()),
    BANNED("banned", """
            Foydalanuvchi blok qilindi!
            """.trim()),
    YOU_ARE_BANNED("you.are.banned", """
            Siz bloklangansiz!
            """.trim()),
    UN_BAN_BUTTON("un.ban.button", """
            Blokdan chiqarish
            """.trim()),
    UN_BANNED("un.banned", """
            Blokdan chiqarildi!
            """.trim()),



    //---------------------------------------BUSINESSMAN MESSAGE KEY---------------------------------------

    ADD_PRODUCT_BUTTON("add.product.button", """
            Mahsulot qo'shish
            """.trim()),

    SETTINGS_BUSINESSMAN("settings.businessman", """
            Sozlamalar menyusi.
            """.trim()),

    BUSINESSMAN_PANEL("businessman.panel", """
            Businessman panel.
            """.trim()),


    ADD_PRODUCT_ENTER_CATEGORY("add.product.enter.category", """
            Kategoriyani tanlang yoki yangi kategoriyani kiriting.
            Yangi kategorya yaratish uchun yangi kategorya nomini kiriting.
            """.trim()),

    ADD_PRODUCT_ENTER_NAME("add.product.enter.name", """
            Mahsulot nomini kiriting.
            """.trim()),

    ADD_PRODUCT_ENTER_PRICE("add.product.enter.price", """
            Mahsulot narxini kiriting.
            """.trim()),

    ADD_PRODUCT_ENTER_AMOUNT("add.product.enter.amount", """
            Mahsulot sonini kiriting.
            """.trim()),

    ADD_PRODUCT_ENTER_PHOTO("add.product.enter.photo", """
            Mahsulot rasmini kiriting.
            """.trim()),

    ADD_PRODUCT_ENTER_DESCRIPTION("add.product.enter.description", """
            Mahsulotning qo'shimcha malumotlarini kiriting.
            """.trim()),

    ADD_PRODUCT_FINISHED("add.product.finished", """
            Mahsulot muvaffaqiyatli qo'shildi.
            """.trim()),
    // properties yozish kk!
    ORDER_MENU("order.menu", """
            Buyurtma berish menyusi.
            """.trim()),

    OPEN_BASKET_BUTTON("open.basket.button", """
            Savatni ko'rish
            """.trim()),
    EDIT_INFORMATION_BUTTON("edit.information", """
            Ma'lumotlarni tahrirlash
            """.trim()),
    CHOOSE_TYPE_OF_EDITING("choose.type.of.editing", """
            Biznes haqida:
                Biznes nomi: %s
                Biznes turi: %s
                Hisob raqami: %s
                Biznes filiallari soni: %s
            """.trim()),
    EDIT_BUSINESS_NAME_BUTTON("edit.business.name.button", """
            Biznes nomini o'zgartirish
            """.trim()),
    EDIT_BUSINESS_LOCATION_BUTTON("edit.business.location.button", """
            Biznes joylashuvini qo'shish
            """.trim()),
    ENTER_NAME_FOR_BUSINESS("enter.name.for.business", """
            Biznes uchun nom kiriting:
            """.trim()),
    ENTER_LOCATION_FOR_BUSINESS("enter.location.for.business", """
            Biznesning joylashuv joyini kiriting:
            """.trim()),
    BUSINESS_NAME_EDITED("business.name.edited", """
            Biznes nomi o'zgartirildi!
            """.trim()),
    BUSINESS_LOCATION_EDITED("business.location.edited", """
            Biznes joylashuvi o'zgartirildi!
            """.trim()),


    ADD_TO_BASKET_BUTTON("add.to.basket", """
            Savatga maxsulot qo'shish
            """.trim()),
    CHOOSE_BUSINESS("choose.business", """
            Biznesni tanlang.
            """.trim()),

    CHOOSE_CATEGORY("choose.category", """
            Kategoriyani tanlang.
            """.trim()),

    MY_BALANCE_BUTTON("my.balance_button", """
            Mening Hisobim
            """.trim()),
    ADD_CARD("add.card", """
            Karta qo'shing:
            """.trim()),
    ADD_CARD_BUTTON("add.card.button", """
            Karta yaratish
            """.trim()),
    GET_MY_BALANCE_BUTTON("get.my.balance.button", """
            Hisobimni ko'rish
            """.trim()),


    MY_BALANCE("my.balance", """
            Mening hisobim.
            """.trim()),
    GET_MY_BALANCE("get.my.balance", """
            Karta raqami: %s
            Hisob: %s
            """.trim()),
    CARD_CREATED("card.created", """
            Karta yaratildi!
            """.trim()),
    ADD_CARD_TO_BUSINESS_BUTTON("add.card.to.business.button", """
            Biznesga karta qo'shish
            """.trim()),

    CHOOSE_PRODUCT("choose.product", """
            Mahsulotni tanlang.
            """.trim()),

    OPEN_PRODUCT("open.product", """
            Mahsulot nomi : %s
            Mahsulot narxi : %s
            Mahsulot soni : %s
            Mahsulotning qo'shimcha ma'lumoti:\n%s
            """.trim()),


    SAVE_BASKET_BUTTON("save.basket.button", """
            Savatga solish
            """.trim()),

    OPEN_BASKET("open.basket", """
            Hurmatli %s.
            ----------------------
            %s
            ----------------------
            Mahsulotlar = %s
            Dostavka = %s
            Service xizmati = %s
            Jami = %s
            """.trim()),

    CONFIRM_ORDER_BUTTON("confirm.order.button", """
            Buyurtmani tasdiqlash
            """.trim()),

    CLEAR_BASKET_BUTTON("clear.basket.button", """
            Savatni tozalash
            """.trim()),

    CREATE_LOCATION("create.location", """
            Joylashuvingizni kiriting.
            """.trim()),


    //---------------------------------------COURIER MESSAGE KEY---------------------------------------

    SETTINGS_COURIER("settings.courier", """
            Sozlamalar menyusi.
            """.trim()),
    ORDER_DELIVERED_BUTTON("order.delivered.button", """
            Yetkazib berishni tugatish
            """.trim()),
    ORDER_DELIVERED("order.delivered", """
            Buyurtma yetkazib berildi!
            """.trim()),
    COURIER_PANEL("courier.panel", """
            Kuryer panel.
            """.trim()),

    YOU_ARE_NOT_BALANCE("you.are.not.balance", """
            Sizda hisob mavjud emas!
            """.trim()),

    BASKET_EMPTY("basket.empty", """
            Savat bo'sh.
            """.trim()),

    ORDER_FINISHED("wait.order.finished", """
            Sizning buyurtmangiz qabul qilindi.
            """.trim()),

    WAIT_ORDER("wait.order", """
            Hozir bo'sh kurier mavjud emas shu sababdan buyurtmangiz bir ozga kechiktiriladi.
            """.trim()),

    ADD_MONEY_TO_CARD("add.money.to.card", """
            Hisobingizga mablag' qo'shish
            minimum : 10000
            maximum : 1000000
            """.trim()),

    NOT_ENOUGH_MONEY("not.enough.money", """
            Sizda %s som mablag' yetmayabdi.
            """.trim()),


    CHOOSE_ADD_PRODUCT_TYPE("choose.add.product.type", """
            Tanlang:
            """.trim()),

    FILL_PRODUCT_BUTTON("fill.product.button", """
            Maxsulot sonini oshirish
            """.trim()),

    FILL_PRODUCT("fill.product", """
            Maxsulot sonini kiriting:
            """.trim()),

    WRONG_AGE("wrong.age", """
            Yoshingiz to'g'ri kelmaydi!
            """.trim()),

    AGE_NOT_ENTERED("age.not.entered", """
            Hali yosh kiritmagan!
            """.trim());




    private String key;
    private String val;

    MessageKey(String key, String val) {
        this.key = key;
        this.val = val;
    }
}
