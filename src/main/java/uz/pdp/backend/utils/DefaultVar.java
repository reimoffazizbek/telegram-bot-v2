package uz.pdp.backend.utils;

import lombok.Getter;
import lombok.Setter;

import java.util.logging.Level;

public class DefaultVar {
    @Getter
    @Setter
    private static Long deliveryPrice = 8000L;
    @Getter
    @Setter
    private static Long servicePrice = 5000L;
}
