package uz.pdp.backend.exceptions;

public class BalanceNotFoundException extends Exception{
    public BalanceNotFoundException() {
    }

    public BalanceNotFoundException(String message) {
        super(message);
    }
}
