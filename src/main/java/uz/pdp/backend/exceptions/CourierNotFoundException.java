package uz.pdp.backend.exceptions;

public class CourierNotFoundException extends Exception {
    public CourierNotFoundException() {
    }

    public CourierNotFoundException(String message) {
        super(message);
    }
}
