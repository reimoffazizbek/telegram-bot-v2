package uz.pdp.backend.exceptions;

public class NotEnoughProductException extends Exception {
    public NotEnoughProductException(String message) {
        super(message);
    }
    public NotEnoughProductException() {
    }
}
