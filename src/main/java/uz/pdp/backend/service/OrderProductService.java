package uz.pdp.backend.service;

import javassist.NotFoundException;
import lombok.Getter;
import uz.pdp.backend.exceptions.NotEnoughProductException;
import uz.pdp.backend.exceptions.OrderNotFoundException;
import uz.pdp.backend.model.Order;
import uz.pdp.backend.model.OrderProduct;
import uz.pdp.backend.model.Product;
import uz.pdp.backend.repository.impl.AuthRepositoryImpl;
import uz.pdp.backend.repository.impl.OrderProductRepositoryImpl;
import uz.pdp.backend.repository.impl.OrderRepositoryImpl;
import uz.pdp.backend.repository.impl.ProductsRepositoryImpl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class OrderProductService {
    @Getter
    private static final OrderProductService instance = new OrderProductService();
    private static final OrderProductRepositoryImpl orderProductRepository = OrderProductRepositoryImpl.getInstance();
    private static final ProductsRepositoryImpl productRepository = ProductsRepositoryImpl.getInstance();
    private static final AuthRepositoryImpl authRepository = AuthRepositoryImpl.getInstance();
    private static final OrderRepositoryImpl orderRepository = OrderRepositoryImpl.getInstance();
    private OrderProductService() {
    }

    public void createOrderProduct(UUID productId, int amount, UUID orderId) throws NotEnoughProductException {
        List<Product> products = productRepository.findAll();
        Optional<Product> first = products.stream().filter(temp -> temp.getId().equals(productId)).findFirst();
        Product product = first.get();
        if (product.getCount() < amount){
            throw new NotEnoughProductException("Not Enough Product!");
        }

        System.out.println(product);

        Optional<OrderProduct> orderProductOptional = orderProductRepository.findAll().stream()
                .filter(temp -> temp.getProductId().equals(productId))
                .filter(temp -> temp.getOrderId().equals(orderId))
                .findFirst();


        OrderProduct orderProduct;

        if(orderProductOptional.isEmpty()){
            orderProduct = new OrderProduct(productId, amount, orderId);
            orderProductRepository.save(orderProduct);
        } else {
            orderProduct = orderProductOptional.get();
            orderProduct.setAmount(orderProduct.getAmount() + amount);
            orderProductRepository.update(orderProduct);
        }

        Order order = orderRepository.findById(orderId).get();
        order.setActualPrice(order.getActualPrice() + (product.getPrice() * amount));
        orderRepository.update(order);
    }

    public List<OrderProduct> getMyAllOrderProduct(UUID orderId) {
        List<OrderProduct> all = orderProductRepository.findAll();
        return all.stream().filter(temp -> temp.getOrderId().equals(orderId)).collect(Collectors.toList());
    }

    public void deleteOrderProduct(UUID orderProductId) throws NotFoundException {
        Optional<OrderProduct> orderProductOptional = orderProductRepository.findById(orderProductId);
        if (orderProductOptional.isEmpty()) {
            throw new NotFoundException("Order product not found with id! ");
        }

        Order order = orderRepository.findById(orderProductOptional.get().getOrderId()).get();
        Product product = productRepository.findById(orderProductOptional.get().getProductId()).get();

        order.setActualPrice(order.getActualPrice() - (orderProductOptional.get().getAmount() * product.getPrice()));
        orderRepository.update(order);

        productRepository.update(product);

        orderProductRepository.delete(orderProductId);
    }
    public void clearOrderProduct(UUID orderId) throws OrderNotFoundException {
        List<OrderProduct> allOrderProducts = orderProductRepository.findAll();
        Optional<Order> orderOptional = orderRepository.findById(orderId);
        if(orderOptional.isEmpty()){
            throw new OrderNotFoundException("Order not found!");
        }
        orderOptional.get().setActualPrice(0);
        orderOptional.get().setDeliveryPrice(0);
        orderOptional.get().setServicePrice(0);
        orderRepository.update(orderOptional.get());
        List<OrderProduct> orderProductsToRemove = allOrderProducts.stream()
                .filter(orderProduct -> orderProduct.getOrderId().equals(orderId))
                .toList();
        orderProductsToRemove.forEach(orderProduct -> {
            orderProductRepository.delete(orderProduct.getId());
        });
    }
}

