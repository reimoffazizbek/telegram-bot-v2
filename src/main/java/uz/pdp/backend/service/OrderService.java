package uz.pdp.backend.service;

import javassist.NotFoundException;
import lombok.Getter;
import uz.pdp.backend.enums.Role;
import uz.pdp.backend.exceptions.BalanceNotFoundException;
import uz.pdp.backend.exceptions.CourierNotFoundException;
import uz.pdp.backend.exceptions.OrderNotFoundException;
import uz.pdp.backend.model.*;
import uz.pdp.backend.repository.impl.*;
import uz.pdp.backend.utils.Bots;
import uz.pdp.backend.utils.DefaultVar;
import uz.pdp.backend.utils.GlobalVar;

import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class OrderService {
    @Getter
    private static final OrderService instance = new OrderService();
    private static final OrderRepositoryImpl orderRepository = OrderRepositoryImpl.getInstance();
    private static final OrderProductRepositoryImpl orderProductRepository = OrderProductRepositoryImpl.getInstance();
    private static final OrderProductService orderProductService = OrderProductService.getInstance();
    private static final AuthRepositoryImpl authRepository = AuthRepositoryImpl.getInstance();
    private static final ProductsRepositoryImpl productsRepository = ProductsRepositoryImpl.getInstance();
    private static final CategoryRepositoryImpl categoryRepository = CategoryRepositoryImpl.getInstance();
    private static final BalanceService balanceService = BalanceService.getInstance();
    private static final BusinessRepositoryImpl businessRepository = BusinessRepositoryImpl.getInstance();

    private OrderService() {
    }

    public Order orderVerify(Long userId) {
        List<Order> orders = orderRepository.findAll();
        Optional<Order> optionalOrder = orders.stream()
                .filter(temp -> temp.getOwnerId().equals(userId))
                .filter(temp -> !temp.isActive() && !temp.isDelete()).findFirst();
        if (optionalOrder.isPresent())
            return optionalOrder.get();

        Order order = new Order(userId);
        order.setServicePrice(DefaultVar.getServicePrice());
        order.setDeliveryPrice(DefaultVar.getDeliveryPrice());
        orderRepository.save(order);
        return order;
    }

    public Order getOrderOfCourier(long courierId) throws NotFoundException {
        Optional<Order> optionalOrder = orderRepository.findAll().stream()
                .filter(temp -> temp.isActive() && !temp.isDelete())
                .filter(temp -> temp.getCourierId()!=null && temp.getCourierId().equals(courierId)).findFirst();
        if (optionalOrder.isEmpty())
            throw new NotFoundException("You do not have order now");

        return optionalOrder.get();
    }

    public List<Order> getMyOrdersHistory(long userId) {
        return orderRepository.findAll().stream()
                .filter(temp -> temp.getOwnerId().equals(userId)).
                filter(Order::isDelete).collect(Collectors.toList());
    }

    public List<Order> getClientsActiveOrders(long userId) {
        return orderRepository.findAll().stream()
                .filter(temp -> temp.getOwnerId().equals(userId)).
                filter(Order::isActive).collect(Collectors.toList());
    }

    public Boolean finishOrder(UUID orderId) throws OrderNotFoundException {
        Optional<Order> optionalOrder = orderRepository.findById(orderId);
        if (optionalOrder.isEmpty())
            throw new OrderNotFoundException("Order not found!");

        Order order = optionalOrder.get();

        List<OrderProduct> myAllOrderProduct = orderProductService.getMyAllOrderProduct(orderId);

        for (OrderProduct orderProduct : myAllOrderProduct) {
            Optional<Product> product = productsRepository.findById(orderProduct.getProductId());
            if(product.isEmpty()) continue;

            product.get().setCount(product.get().getOriginCount() - orderProduct.getAmount());
            System.out.println(product.get());
            productsRepository.update(product.get());
            orderProductRepository.delete(orderProduct.getId());
        }



        try {
            Balance fromBalance = balanceService.getBalance(order.getOwnerId());
            for (OrderProduct orderProduct : myAllOrderProduct) {
                Optional<Product> product = productsRepository.findById(orderProduct.getProductId());
                if(product.isEmpty()) continue;
                Optional<Category> category = categoryRepository.findById(product.get().getCategoryID());
                if(category.isEmpty()) continue;
                Optional<Business> business = businessRepository.findById(category.get().getBusinessId());
                if(business.isEmpty()) continue;
                balanceService.transferMoney(fromBalance.getCardNumber(), business.get().getBalanceCardNumber(), orderProduct.getAmount() * product.get().getPrice());
            }
            Balance courierBalance = balanceService.getBalance(order.getCourierId());
            balanceService.transferMoney(fromBalance.getCardNumber(), courierBalance.getCardNumber(), order.getDeliveryPrice());
            Balance serviceBalance = balanceService.getBalance(Bots.ADMINS.get(0));
            balanceService.transferMoney(fromBalance.getCardNumber(), serviceBalance.getCardNumber(), order.getServicePrice());
        } catch (BalanceNotFoundException | NotFoundException e) {
            GlobalVar.log(Level.SEVERE, e.getMessage(), e);
        } catch (IllegalArgumentException e){
            throw new IllegalArgumentException(e);
        }

        order.setActive(false);
        order.setDelete(true);
        orderRepository.update(order);
        return true;
    }

    public Boolean startOrder(UUID orderId) throws OrderNotFoundException {
        Optional<Order> optionalOrder = orderRepository.findById(orderId);
        if (optionalOrder.isEmpty())
            throw new OrderNotFoundException("Order not found!");

        Order order = optionalOrder.get();
        order.setActive(true);
        orderRepository.update(order);
        return true;
    }

    public Order giveOrderToCourier(UUID orderId) throws OrderNotFoundException, CourierNotFoundException {
        List<Order> orders = orderRepository.findAll();
        Optional<Order> optionalOrder = orders.stream().filter(temp -> temp.getId().equals(orderId)).findFirst();
        if (optionalOrder.isEmpty())
            throw new OrderNotFoundException("OrderNotFound!");

        Order order = optionalOrder.get();
        Optional<User> compare = compare();
        if (compare.isEmpty()) throw new CourierNotFoundException("Courier not found!");
        User user = compare.get();
        order.setCourierId(user.getId());
        orderRepository.update(order);
        return order;
    }

    public Optional<User> compare() {
        List<User> users = authRepository.findAll();
        List<Order> orders = orderRepository.findAll().stream().filter(temp -> !temp.isActive() && temp.isDelete()).toList();
        List<Order> activeOrders = orderRepository.findAll().stream().filter(temp -> temp.isActive() && !temp.isDelete()).toList();


        return users.stream()
                .filter(temp -> {
                    try {
                        balanceService.getBalance(temp.getId());
                        return temp.getRole() == Role.COURIER && !temp.isBan();
                    } catch (BalanceNotFoundException e) {
                        return false;
                    }
                })
                .filter(temp -> activeOrders.stream().noneMatch(activeOrder -> activeOrder.getCourierId() != null && activeOrder.getCourierId().equals(temp.getId())))
                .min((temp, temp2) -> orders.stream().filter(order -> order.getCourierId().equals(temp.getId())).toList().size()
                        -
                        orders.stream().filter(order -> order.getCourierId().equals(temp2.getId())).toList().size());

    }


}
