package uz.pdp.backend.model;

import lombok.Data;
import org.telegram.telegrambots.meta.api.objects.Location;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class Order implements Serializable {
    private final UUID id = UUID.randomUUID();
    private final Long ownerId;
    private Long courierId;
    private long deliveryPrice;
    private long actualPrice;
    private long servicePrice;
    private List<Location> locationsFrom = new ArrayList<>();
    private Location locationTo;
    private boolean isActive = false;
    private boolean isDelete = false;

    public Order(Long ownerId) {
        this.ownerId = ownerId;
    }
}
