package uz.pdp.backend.model;

import lombok.Data;
import uz.pdp.backend.repository.impl.OrderProductRepositoryImpl;
import uz.pdp.backend.service.OrderProductService;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Data
public class Product implements Serializable {
    private final UUID id = UUID.randomUUID();
    private String productName;
    private Long price;
    private String description;
    private String photoUrl;
    private int count;
    private UUID categoryID;

    public Product(String productName, Long price, String description, String photoUrl, int count, UUID categoryID) {
        this.productName = productName;
        this.price = price;
        this.description = description;
        this.photoUrl = photoUrl;
        this.count = count;
        this.categoryID = categoryID;
    }

    public int getCount() {
        List<OrderProduct> all = OrderProductRepositoryImpl.getInstance().findAll();
        List<OrderProduct> list = all.stream().filter(temp -> temp.getProductId().equals(id) && temp.getAmount() > 0).toList();

        int tempCount = count;
        for (OrderProduct orderProduct : list) {
            tempCount -= orderProduct.getAmount();
        }
        return tempCount;
    }

    public int getOriginCount() {
        return count;
    }
}
