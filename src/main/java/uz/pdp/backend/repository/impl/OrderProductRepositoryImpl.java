package uz.pdp.backend.repository.impl;
import com.google.gson.reflect.TypeToken;
import lombok.Getter;
import lombok.NonNull;
import uz.pdp.backend.model.OrderProduct;
import uz.pdp.backend.repository.BaseRepository;
import uz.pdp.backend.service.FileHelper;
import uz.pdp.backend.utils.FIleURLS;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class OrderProductRepositoryImpl implements BaseRepository<OrderProduct , UUID> {
    @Getter
    private static final OrderProductRepositoryImpl instance = new OrderProductRepositoryImpl();
    @Override
    public Boolean save(OrderProduct orderProduct) {
        List<OrderProduct> allOrderProductFromFile = getAllOrderProductFromFile();
        allOrderProductFromFile.add(orderProduct);
        setAllOrderProductsFromFile(allOrderProductFromFile);
        return true;
        
    }

    @Override
    public Boolean update(OrderProduct orderProduct) {
        List<OrderProduct> collect = getAllOrderProductFromFile().stream().map(temp -> {
            if (temp.getId().equals(orderProduct.getId()))
                return orderProduct;
            return temp;
        }).collect(Collectors.toList());
        setAllOrderProductsFromFile(collect);
        return true;
    }
    public void delete (UUID orderProduct) {
        List<OrderProduct> allOrderProducts = getAllOrderProductFromFile();
        allOrderProducts.removeIf(temp -> temp.getId().equals(orderProduct));
        setAllOrderProductsFromFile(allOrderProducts);
    }
    public void deleteAll (OrderProduct orderProduct){
        List<OrderProduct> allOrderProducts = getAllOrderProductFromFile();
        allOrderProducts.removeIf(temp -> temp.equals(orderProduct));
        setAllOrderProductsFromFile(allOrderProducts);
    }
    @Override
    public List<OrderProduct> findAll() {
       return getAllOrderProductFromFile();
    }

    @Override
    public Optional<OrderProduct> findById(UUID id) {
        return getAllOrderProductFromFile().stream().filter(orderProduct -> orderProduct.getId().equals(id) ).findFirst();
    }
    
    
    
    @NonNull
    private List<OrderProduct> getAllOrderProductFromFile() {
        List<OrderProduct> load = FileHelper.load(FIleURLS.ORDER_PRODUCTS, new TypeToken<List<OrderProduct>>() {
        }.getType());
        return load == null ? new ArrayList<>() : load;
    }
    private void setAllOrderProductsFromFile(List<OrderProduct> data) {
        FileHelper.write(FIleURLS.ORDER_PRODUCTS, data);
    }
}
