package uz.pdp.backend.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import uz.pdp.backend.model.BusinessApplication;
import uz.pdp.backend.model.CourierApplication;

@Builder
@AllArgsConstructor
@Data
public class RejectCourierApplicationDTO {
    private CourierApplication courierApplication;
    private String rejectReason;
}
