package uz.pdp.bot.utils;

public interface TempDataKeys {
    String BUSINESS_NAME = "business.name";
    String BUSINESS_TYPE = "business.type";
    String BUSINESS_LOCATION = "business.location";
    String BUSINESS_APPLICATION = "business.application";
    String COURIER_APPLICATION = "courier.application";
    String CURRENT_BUSINESS_ID = "current.business.id";
    String CURRENT_CATEGORY_ID = "current.category.id";
    String CURRENT_PRODUCT_ID = "current.product.id";
    String ADD_PRODUCT_CATEGORY_NAME = "add.product.category.name";
    String ADD_PRODUCT_PRODUCT_NAME = "add.product.product.name";
    String ADD_PRODUCT_PRODUCT_PRICE = "add.product.product.price";
    String ADD_PRODUCT_PRODUCT_AMOUNT = "add.product.product.amount";
    String ADD_PRODUCT_PRODUCT_PHOTO = "add.product.product.photo";
    String ADD_PRODUCT_PRODUCT_DESCRIPTION = "add.product.product.description";
    String USER_ID_FOR_BAN = "user.id.for.ban";
    String PRODUCT_COUNT = "product.count";
}
