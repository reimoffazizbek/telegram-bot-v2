package uz.pdp.bot;

import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import uz.pdp.backend.exceptions.*;
import uz.pdp.backend.repository.impl.CategoryRepositoryImpl;
import uz.pdp.backend.repository.impl.OrderRepositoryImpl;
import uz.pdp.backend.service.*;
import uz.pdp.backend.utils.Bots;


public class Main {
    public static void main(String[] args) throws BusinessNotFoundException, WrongRoleException, OrderNotFoundException {
        MyBot myBot = new MyBot(Bots.TOKEN);

        try {
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
            telegramBotsApi.registerBot(myBot);
        } catch (TelegramApiException e) {
            throw new RuntimeException(e);
        }
    }
}