package uz.pdp.bot.service;

import javassist.NotFoundException;
import org.telegram.telegrambots.meta.api.objects.*;
import uz.pdp.backend.enums.Role;
import uz.pdp.backend.enums.Status;
import uz.pdp.backend.model.User;
import uz.pdp.backend.repository.impl.AuthRepositoryImpl;
import uz.pdp.backend.service.AuthService;
import uz.pdp.backend.service.BalanceService;
import uz.pdp.backend.utils.GlobalVar;
import uz.pdp.backend.utils.MessageKey;
import uz.pdp.bot.MyBot;
import uz.pdp.bot.repository.TempDataRepository;

import java.util.logging.Level;

import static uz.pdp.bot.service.ResponseService.sendMessage;


public class BotService {
    private MyBot bot;
    private final AuthService authService = AuthService.getInstance();
    private final AuthRepositoryImpl authRepository = AuthRepositoryImpl.getInstance();
    private final TempDataRepository tempDataRepository = TempDataRepository.getInstance();

    public BotService(MyBot bot) {
        this.bot = bot;
    }



    public void onUpdateReceived(Update update){
        GlobalVar.setMyBot(bot);
        Message message;
        if(update.hasMessage()){
            message = update.getMessage();
        } else if(update.hasCallbackQuery()){
            message = update.getCallbackQuery().getMessage();
            message.setText(update.getCallbackQuery().getData());
        } else
            return;
        User user = authService.userVerify(message.getChat());
        GlobalVar.setUSER(user);
        if(user.getRole() == Role.ADMIN) BalanceService.getInstance().createBalance(user.getId());
        authRepository.update(user);

        if (user.isBan()){
            sendMessage(message.getChatId(), MessageKey.YOU_ARE_BANNED);
            return;
        }

        if(user.getRole() == Role.CLIENT) clientCases(update, message, user.getStatus());
        else if(user.getRole() == Role.ADMIN) adminCases(update, message, user.getStatus());
        else if(user.getRole() == Role.BUSINESSMAN) businessmanCases(update, message, user.getStatus());
        else if(user.getRole() == Role.COURIER) courierCases(update, message, user.getStatus());
    }

    private void courierCases(Update update, Message message, Status status) {
        switch (status){
            case STARTED -> {
                sendMessage(message.getChatId(), "mainMenu", ButtonService.createCourierPanelButtons());

                User user = GlobalVar.getUSER();
                user.setStatus(Status.MAIN_MENU);
                authRepository.update(user);
            }
            case MAIN_MENU -> CourierMenuService.mainMenu(update, message);
            case SETTINGS -> CourierMenuService.settingsMenu(update, message);
            case SET_LANGUAGE -> CourierMenuService.setLanguage(update, message);
            case MY_BALANCE -> CourierMenuService.myBalance(update, message);
        }
    }

    private void businessmanCases(Update update, Message message, Status status) {
        switch (status){
            case STARTED -> {
                sendMessage(message.getChatId(), "mainMenu", ButtonService.createBusinessmanPanelButtons());
                User user = GlobalVar.getUSER();
                user.setStatus(Status.MAIN_MENU);
                authRepository.update(user);
            }
            case MAIN_MENU -> BusinessmanMenuService.mainMenu(update, message);
            case SETTINGS -> BusinessmanMenuService.settingsMenu(update, message);
            case SET_LANGUAGE -> BusinessmanMenuService.setLanguage(update, message);
            case ADD_PRODUCT_BUSINESSMAN -> BusinessmanMenuService.addProduct(update, message);
            case CHOOSE_TYPE_OF_EDITING -> BusinessmanMenuService.chooseTypeOfEditing(update, message);
            case EDIT_BUSINESS_NAME -> BusinessmanMenuService.editBusinessName(update, message);
            case EDIT_BUSINESS_LOCATION -> BusinessmanMenuService.editBusinessLocation(update, message);
            case MY_BALANCE -> BusinessmanMenuService.myBalance(update, message);
            case CHOOSE_ADD_PRODUCT_TYPE_BUSINESSMAN -> BusinessmanMenuService.chooseAddProductType(update, message);
            case CHOOSE_CATEGORY -> BusinessmanMenuService.chooseCategory(update, message);
            case CHOOSE_PRODUCT -> BusinessmanMenuService.chooseProduct(update, message);
            case OPEN_PRODUCT_FILL_COUNT -> BusinessmanMenuService.openProductAndFillCount(update, message);
        }
    }

    public void clientCases(Update update, Message message, Status userStatus){
        switch (userStatus){
            case STARTED -> RegistrationService.started(update, message);
            case CHOOSE_LANGUAGE -> RegistrationService.chooseLanguage(update, message);
            case SHARE_PHONE_NUMBER -> RegistrationService.sharePhoneNumber(update, message);
            case MAIN_MENU -> ClientMenuService.mainMenu(update, message);
            case SETTINGS -> ClientMenuService.settingsMenu(update, message);
            case SET_LANGUAGE -> ClientMenuService.setLanguage(update, message);
            case CHOOSE_APPLICATION_TYPE_MENU -> ClientMenuService.chooseApplicationType(update, message);
            case BUSINESS_APPLICATION_MENU -> ClientMenuService.businessApplicationMenu(update, message);
            case COURIER_APPLICATION_MENU -> ClientMenuService.courierApplicationMenu(update, message);
            case ORDER_MENU -> ClientMenuService.orderMenu(update, message);
            case CHOOSE_BUSINESS -> ClientMenuService.chooseBusiness(update, message);
            case CHOOSE_CATEGORY -> ClientMenuService.chooseCategory(update, message);
            case CHOOSE_PRODUCT -> ClientMenuService.chooseProduct(update, message);
            case OPEN_PRODUCT -> ClientMenuService.openProduct(update, message);
            case OPEN_BASKET -> ClientMenuService.openBasket(update, message);
            case CREATE_LOCATION -> ClientMenuService.createMyLocationOpenBasketMenu(update, message);
            case ADD_CARD -> ClientMenuService.addCard(update, message);
            case ADD_MONEY_TO_CARD -> ClientMenuService.addMoneyToCard(update, message);
        }
    }
    public void adminCases(Update update, Message message, Status userStatus){
        switch (userStatus){
            case STARTED -> AdminMenuService.started(update, message);
            case CHOOSE_LANGUAGE -> AdminMenuService.chooseLanguage(update, message);
            case MAIN_MENU -> AdminMenuService.mainMenu(update, message);
            case SETTINGS -> AdminMenuService.settingsMenu(update, message);
            case SET_LANGUAGE -> AdminMenuService.setLanguage(update, message);
            case SET_PHONE_NUMBER -> AdminMenuService.setPhoneNumber(update, message);
            case CHECK_BUSINESS_APPLICATION_MENU -> AdminMenuService.checkBusinessApplications(update, message);
            case CHECK_COURIER_APPLICATION_MENU -> AdminMenuService.checkCourierApplications(update, message);
            case OPEN_BUSINESS_APPLICATION -> AdminMenuService.openBusinessApplication(update, message);
            case OPEN_COURIER_APPLICATION -> AdminMenuService.openCourierApplication(update, message);
            case WRITING_COMMENT_TO_BUSINESSMAN_MENU -> AdminMenuService.writingCommentToBusinessmanMenu(update, message);
            case WRITING_COMMENT_TO_COURIER_MENU -> AdminMenuService.writingCommentToCourierMenu(update, message);
            case CHOOSE_APPLICATION_TYPE_MENU -> AdminMenuService.choosingApplicationTypeMenu(update, message);
            case CHECK_REPORTS -> AdminMenuService.checkReportsMenu(update, message);
            case SEARCH_USER -> AdminMenuService.searchUser(update, message);
            case CHECKING_USER_INFO -> AdminMenuService.checkingUserInfo(update, message);
        }
    }
}
