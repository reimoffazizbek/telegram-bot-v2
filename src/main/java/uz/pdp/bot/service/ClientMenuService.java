package uz.pdp.bot.service;

import javassist.NotFoundException;
import org.telegram.telegrambots.meta.api.objects.Location;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import uz.pdp.backend.enums.BusinessType;
import uz.pdp.backend.enums.Language;
import uz.pdp.backend.enums.Status;
import uz.pdp.backend.exceptions.*;
import uz.pdp.backend.model.*;
import uz.pdp.backend.payload.CreateBusinessApplicationDTO;
import uz.pdp.backend.repository.impl.*;
import uz.pdp.backend.service.*;
import uz.pdp.backend.utils.GlobalVar;
import uz.pdp.backend.utils.MessageKey;
import uz.pdp.backend.utils.Regexes;
import uz.pdp.bot.MyBot;
import uz.pdp.bot.model.TempData;
import uz.pdp.bot.repository.TempDataRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.regex.Pattern;

import static uz.pdp.backend.utils.MessageKey.*;
import static uz.pdp.bot.service.ResponseService.*;
import static uz.pdp.bot.utils.TempDataKeys.*;

public class ClientMenuService {
    private static final AuthRepositoryImpl authRepository = AuthRepositoryImpl.getInstance();
    private static final I18nService i18nService = I18nService.getInstance();
    private static final TempDataRepository tempDataRepository = TempDataRepository.getInstance();
    private static final BusinessApplicationService businessAppService = BusinessApplicationService.getInstance();
    private static final CourierApplicationService courierAppService = CourierApplicationService.getInstance();
    private static final BusinessService businessService = BusinessService.getInstance();
    private static final CategoryService categoryService = CategoryService.getInstance();
    private static final ProductService productService = ProductService.getInstance();
    private static final ProductsRepositoryImpl productRepository = ProductsRepositoryImpl.getInstance();
    private static final OrderService orderService = OrderService.getInstance();
    private static final OrderProductService orderProductService = OrderProductService.getInstance();
    private static final BalanceService balanceService = BalanceService.getInstance();
    private static final CategoryRepositoryImpl categoryRepository = CategoryRepositoryImpl.getInstance();
    private static final BusinessLocationService businessLocationService = BusinessLocationService.getInstance();
    private static final OrderRepositoryImpl orderRepository = OrderRepositoryImpl.getInstance();
    private static final ExecutorService executor = Executors.newCachedThreadPool();

    public static void mainMenu(Update update, Message message) {
        User user = GlobalVar.getUSER();
        if (message.getText().equals(i18nService.getMsg(SETTINGS_BUTTON))) {
            user.setStatus(Status.SETTINGS);
            sendMessage(message.getChatId(), SETTINGS, ButtonService.createClientSettingsButtons());
        } else if (message.getText().equals(i18nService.getMsg(ORDER_BUTTON))) {
            user.setStatus(Status.ORDER_MENU);
            Order order = orderService.orderVerify(user.getId());
            sendMessage(message.getChatId(), ORDER_MENU, ButtonService.createClientOrderButton());
        } else if (!checkUserActiveApplication(user.getId()) && message.getText().equals(i18nService.getMsg(SUBMIT_APPLICATION_BUTTON))) {
            user.setStatus(Status.CHOOSE_APPLICATION_TYPE_MENU);
            sendMessage(message.getChatId(), CHOOSE_APPLICATION_TYPE, ButtonService.createClientApplicationTypeButton());
        } else {
            sendMessage(message.getChatId(), INFORMATION_ERROR);
            return;
        }
        authRepository.update(user);
    }

    public static void settingsMenu(Update update, Message message) {
        User user = GlobalVar.getUSER();
        if (message.getText().equals(i18nService.getMsg(SET_LANGUAGE_BUTTON))) {
            user.setStatus(Status.SET_LANGUAGE);
            sendMessage(message.getChatId(), SET_LANGUAGE, ButtonService.createLanguageButton());
        } else if (message.getText().equals(i18nService.getMsg(BACK))) {
            user.setStatus(Status.MAIN_MENU);
            sendMessage(message.getChatId(), MAIN_MENU, ButtonService.createClientMainMenu());
        } else {
            sendMessage(message.getChatId(), INFORMATION_ERROR);
            return;
        }
        authRepository.update(user);
    }

    public static void setLanguage(Update update, Message message) {
        if (!update.hasCallbackQuery()) {
            sendMessage(message.getChatId(), i18nService.getMsg(INFORMATION_ERROR));
            return;
        }

        User user = GlobalVar.getUSER();
        findLanguage(message.getText(), user);
        authRepository.update(user);


        user.setStatus(Status.SETTINGS);
        authRepository.update(user);
        editMessage(message.getChatId(), message.getMessageId(), SET_LANGUAGE);
        sendMessage(message.getChatId(), SETTINGS, ButtonService.createClientSettingsButtons());
    }

    public static void businessApplicationMenu(Update update, Message message) {
        if (!tempDataRepository.contains(BUSINESS_NAME)) {
            tempDataRepository.createData(new TempData<>(BUSINESS_NAME, message.getText()));
            sendMessage(message.getChatId(), BUSINESS_APPLICATION_ENTER_TYPE, ButtonService.createBusinessTypeButton());
        } else if (!tempDataRepository.contains(BUSINESS_TYPE)) {
            Optional<BusinessType> businessType = findBusinessType(message.getText());
            if (businessType.isEmpty()) {
                sendMessage(message.getChatId(), INFORMATION_ERROR);
                return;
            }
            tempDataRepository.createData(new TempData<>(BUSINESS_TYPE, businessType.get()));
            sendMessage(message.getChatId(), BUSINESS_APPLICATION_ENTER_LOCATION, ButtonService.createShareLocation());
        } else if (!tempDataRepository.contains(BUSINESS_LOCATION)) {
            if (!message.hasLocation()) {
                sendMessage(message.getChatId(), INFORMATION_ERROR);
                return;
            }
            tempDataRepository.createData(new TempData<>(BUSINESS_LOCATION, message.getLocation()));

            User user = GlobalVar.getUSER();
            try {
                CreateBusinessApplicationDTO createBusinessApplicationDTO = CreateBusinessApplicationDTO.builder()
                        .businessName((String) tempDataRepository.getAndDelete(BUSINESS_NAME).getVal())
                        .businessType((BusinessType) tempDataRepository.getAndDelete(BUSINESS_TYPE).getVal())
                        .location((Location) tempDataRepository.getAndDelete(BUSINESS_LOCATION).getVal())
                        .ownerId(user.getId())
                        .build();

                businessAppService.createBusinessApplication(createBusinessApplicationDTO);

                user.setStatus(Status.MAIN_MENU);
                authRepository.update(user);
                sendMessage(message.getChatId(), FINISHED_BUSINESS_APPLICATION, ButtonService.createClientMainMenu());
            } catch (ClassCastException e) {
                GlobalVar.log(Level.WARNING, "Class cast error", e);
                user.setStatus(Status.MAIN_MENU);
                tempDataRepository.deleteMyAllData();
                authRepository.update(user);
                sendMessage(message.getChatId(), MAIN_MENU, ButtonService.createClientMainMenu());
            }

        }
    }

    private static Optional<BusinessType> findBusinessType(String msg) {
        BusinessType[] values = BusinessType.values();
        for (BusinessType value : values) {
            if (value.getButtonName().equals(msg)) {
                return Optional.of(value);
            }
        }
        return Optional.empty();
    }

    private static void findLanguage(String text, User user) {
        Language[] values = Language.values();
        for (Language value : values) {
            if (value.getCallbackData().equals(text)) {
                user.setLanguage(value);
                return;
            }
        }
    }

    private static boolean checkUserActiveApplication(Long userId) {
        return businessAppService.checkMyActiveApplication(userId) || CourierApplicationService.getInstance().checkMyActiveApplication(userId);
    }

    public static void chooseApplicationType(Update update, Message message) {
        User user = GlobalVar.getUSER();
        if (message.getText().equals(i18nService.getMsg(BUSINESS_APPLICATION_BUTTON))) {
            user.setStatus(Status.BUSINESS_APPLICATION_MENU);
            sendMessage(message.getChatId(), BUSINESS_APPLICATION_ENTER_NAME);
        } else if (message.getText().equals(i18nService.getMsg(COURIER_APPLICATION_BUTTON))) {
            user.setStatus(Status.COURIER_APPLICATION_MENU);
            if (user.getBirthDate() == null)
                sendMessage(message.getChatId(), COURIER_APPLICATION_ENTER_BIRTH_DATE);
            else
                sendMessage(message.getChatId(), COURIER_APPLICATION_ENTER_BIRTH_DATE, ButtonService.createClientOldAgeButton());
        } else if (message.getText().equals(i18nService.getMsg(BACK))) {
            user.setStatus(Status.MAIN_MENU);
            sendMessage(message.getChatId(), MAIN_MENU, ButtonService.createClientMainMenu());
        } else {
            sendMessage(message.getChatId(), INFORMATION_ERROR);
            return;
        }
        authRepository.update(user);
    }

    public static void courierApplicationMenu(Update update, Message message) {
        User user = GlobalVar.getUSER();
        LocalDate birthDate;
        if (user.getBirthDate() != null && update.hasCallbackQuery()) {
            if (message.getText().equals(GlobalVar.getUSER().getBirthDate().format(DateTimeFormatter.ISO_DATE))) {
                birthDate = user.getBirthDate();
                deleteMessage(message.getChatId(), message.getMessageId());
            } else {
                sendMessage(message.getChatId(), INFORMATION_ERROR);
                return;
            }
        } else if (Pattern.compile(Regexes.BIRTH_DATE).matcher(message.getText()).matches()) {
            birthDate = LocalDate.parse(message.getText(), DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        } else {
            sendMessage(message.getChatId(), INFORMATION_ERROR);
            return;
        }
        user.setBirthDate(birthDate);
        authRepository.update(user);
        try {
            courierAppService.createApplication(user.getId());
        } catch (IllegalArgumentException e) {
            user.setStatus(Status.CHOOSE_APPLICATION_TYPE_MENU);
            authRepository.update(user);
            sendMessage(message.getChatId(), MessageKey.WRONG_AGE);
            sendMessage(message.getChatId(), CHOOSE_APPLICATION_TYPE, ButtonService.createClientApplicationTypeButton());
            return;
        } catch (UserNotFoundException e) {
            GlobalVar.log(Level.WARNING, e.getMessage(), e);
        }
        user.setStatus(Status.MAIN_MENU);
        authRepository.update(user);
        sendMessage(message.getChatId(), MAIN_MENU, ButtonService.createClientMainMenu());
    }

    public static void orderMenu(Update update, Message message) {
        User user = GlobalVar.getUSER();

        if (message.getText().equals(i18nService.getMsg(ADD_TO_BASKET_BUTTON))) {
            user.setStatus(Status.CHOOSE_BUSINESS);
            sendMessage(message.getChatId(), CHOOSE_BUSINESS, ButtonService.createClientAllBusinessButton());
        } else if (message.getText().equals(i18nService.getMsg(OPEN_BASKET_BUTTON))) {
            Order order = orderService.orderVerify(user.getId());
            if (orderProductService.getMyAllOrderProduct(order.getId()).isEmpty()) {
                sendMessage(message.getChatId(), BASKET_EMPTY, ButtonService.createClientOrderButton());
                return;
            }
            user.setStatus(Status.OPEN_BASKET);
            sendMessage(message.getChatId(), i18nService.getMsg(OPEN_BASKET).formatted(
                    GlobalVar.getUSER().getFullName(),
                    findOrderProducts(order.getId()),
                    order.getActualPrice(),
                    order.getDeliveryPrice(),
                    order.getServicePrice(),
                    order.getServicePrice() + order.getActualPrice() + order.getDeliveryPrice()
            ), ButtonService.createClientOpenBasket());
        } else if (message.getText().equals(i18nService.getMsg(BACK))) {
            user.setStatus(Status.MAIN_MENU);
            sendMessage(message.getChatId(), MAIN_MENU, ButtonService.createClientMainMenu());
        } else {
            sendMessage(message.getChatId(), INFORMATION_ERROR);
            return;
        }
        authRepository.update(user);
    }

    private static String findOrderProducts(UUID orderId) {
        StringJoiner stringJoiner = new StringJoiner(",\n");
        List<OrderProduct> myAllOrderProduct = orderProductService.getMyAllOrderProduct(orderId);
        myAllOrderProduct.forEach(new Consumer<OrderProduct>() {
            private int i = 1;
            Product product;

            @Override
            public void accept(OrderProduct temp) {
                try {
                    product = productRepository.findById(temp.getProductId()).get();
                    stringJoiner.add(i++ + ". %s -> %s * %s = %s".formatted(product.getProductName(), temp.getAmount(), product.getPrice(), product.getPrice() * temp.getAmount()));
                } catch (Exception e) {
                    try {
                        orderProductService.deleteOrderProduct(temp.getId());
                    } catch (NotFoundException ignored) {
                    }
                }
            }
        });
        return stringJoiner.toString();
    }

    public static void chooseBusiness(Update update, Message message) {
        User user = GlobalVar.getUSER();

        if (!update.hasCallbackQuery()) {
            sendMessage(message.getChatId(), INFORMATION_ERROR);
            return;
        }

        if (message.getText().equals(i18nService.getMsg(BACK))) {
            user.setStatus(Status.ORDER_MENU);
            authRepository.update(user);
            deleteMessage(message.getChatId(), message.getMessageId());
            sendMessage(message.getChatId(), ORDER_MENU, ButtonService.createClientOrderButton());
            return;
        }

        List<Business> allBusinesses = businessService.getAllBusinesses();

        for (Business business : allBusinesses) {
            if (business.getId().toString().equals(message.getText())) {
                user.setStatus(Status.CHOOSE_CATEGORY);
                authRepository.update(user);
                tempDataRepository.createData(new TempData<>(CURRENT_BUSINESS_ID, business.getId()));
                editMessage(message.getChatId(), message.getMessageId(), CHOOSE_CATEGORY, ButtonService.createClientAllBusinessCategoryButton(business.getId()));
                return;
            }
        }
        sendMessage(message.getChatId(), INFORMATION_ERROR);
    }

    public static void chooseCategory(Update update, Message message) {
        User user = GlobalVar.getUSER();
        if (!update.hasCallbackQuery()) {
            sendMessage(message.getChatId(), INFORMATION_ERROR);
            return;
        }

        if (message.getText().equals(i18nService.getMsg(BACK))) {
            user.setStatus(Status.CHOOSE_BUSINESS);
            tempDataRepository.deleteData(CURRENT_BUSINESS_ID);
            authRepository.update(user);
            editMessage(message.getChatId(), message.getMessageId(), CHOOSE_BUSINESS, ButtonService.createClientAllBusinessButton());
            return;
        }

        UUID businessId = (UUID) tempDataRepository.get(CURRENT_BUSINESS_ID).getVal();

        List<Category> categories = categoryService.getAllCategoriesOfBusiness(businessId);

        for (Category category : categories) {
            if (message.getText().equals(category.getId().toString())) {
                user.setStatus(Status.CHOOSE_PRODUCT);
                authRepository.update(user);
                tempDataRepository.createData(new TempData<>(CURRENT_CATEGORY_ID, category.getId()));
                editMessage(message.getChatId(), message.getMessageId(), CHOOSE_PRODUCT, ButtonService.createClientCategoryProductButton(category.getId()));
                return;
            }
        }

    }

    public static void chooseProduct(Update update, Message message) {
        User user = GlobalVar.getUSER();
        if (!update.hasCallbackQuery()) {
            sendMessage(message.getChatId(), INFORMATION_ERROR);
            return;
        }

        if (message.getText().equals(i18nService.getMsg(BACK))) {
            user.setStatus(Status.CHOOSE_CATEGORY);
            tempDataRepository.deleteData(CURRENT_CATEGORY_ID);
            UUID businessId = (UUID) tempDataRepository.get(CURRENT_BUSINESS_ID).getVal();
            authRepository.update(user);
            editMessage(message.getChatId(), message.getMessageId(), CHOOSE_CATEGORY, ButtonService.createClientAllBusinessCategoryButton(businessId));
            return;
        }

        UUID categoryId = (UUID) tempDataRepository.get(CURRENT_CATEGORY_ID).getVal();

        List<Product> allProducts = productService.getCategoriesAllActiveProducts(categoryId);
        for (Product product : allProducts) {
            if (product.getId().toString().equals(message.getText())) {
                tempDataRepository.createData(new TempData<>(PRODUCT_COUNT, 1));
                tempDataRepository.createData(new TempData<>(CURRENT_PRODUCT_ID, product.getId()));
                user.setStatus(Status.OPEN_PRODUCT);
                authRepository.update(user);
                deleteMessageAndSendPhoto(message.getChatId(), message.getMessageId(), product.getPhotoUrl(), i18nService.getMsg(OPEN_PRODUCT).formatted(
                        product.getProductName(),
                        product.getPrice(),
                        product.getCount(),
                        product.getDescription()
                ), ButtonService.createClientOpenProduct((int) tempDataRepository.get(PRODUCT_COUNT).getVal()));
                return;
            }
        }
    }

    public static void openProduct(Update update, Message message) {
        User user = GlobalVar.getUSER();
        if (!update.hasCallbackQuery()) {
            sendMessage(message.getChatId(), INFORMATION_ERROR);
            return;
        }

        if (message.getText().equals(i18nService.getMsg(BACK))) {
            tempDataRepository.deleteData(PRODUCT_COUNT);
            user.setStatus(Status.CHOOSE_PRODUCT);
            tempDataRepository.deleteData(CURRENT_PRODUCT_ID);
            UUID categoryId = (UUID) tempDataRepository.get(CURRENT_CATEGORY_ID).getVal();
            authRepository.update(user);
            deletePhotoAndSendMessage(message.getChatId(), message.getMessageId(), CHOOSE_PRODUCT, ButtonService.createClientCategoryProductButton(categoryId));
            return;
        } else if (message.getText().equals("+")) {
            int productCount = (int) tempDataRepository.getAndDelete(PRODUCT_COUNT).getVal();
            UUID productId = (UUID) tempDataRepository.get(CURRENT_PRODUCT_ID).getVal();
            Product product = productRepository.findById(productId).get();
            if (productCount >= product.getCount()) {
                tempDataRepository.createData(new TempData<>(PRODUCT_COUNT, productCount));
                return;
            }
            productCount++;
            tempDataRepository.createData(new TempData<>(PRODUCT_COUNT, productCount));
            editDescription(message.getChatId(), message.getMessageId(), i18nService.getMsg(OPEN_PRODUCT).formatted(
                    product.getProductName(),
                    product.getPrice(),
                    product.getCount(),
                    product.getDescription()
            ), ButtonService.createClientOpenProduct((int) tempDataRepository.get(PRODUCT_COUNT).getVal()));
        } else if (message.getText().equals("-")) {
            int productCount = (int) tempDataRepository.getAndDelete(PRODUCT_COUNT).getVal();
            if (productCount <= 1) {
                tempDataRepository.createData(new TempData<>(PRODUCT_COUNT, productCount));
                return;
            }
            productCount--;
            UUID productId = (UUID) tempDataRepository.get(CURRENT_PRODUCT_ID).getVal();
            Product product = productRepository.findById(productId).get();
            tempDataRepository.createData(new TempData<>(PRODUCT_COUNT, productCount));
            editDescription(message.getChatId(), message.getMessageId(), i18nService.getMsg(OPEN_PRODUCT).formatted(
                    product.getProductName(),
                    product.getPrice(),
                    product.getCount(),
                    product.getDescription()
            ), ButtonService.createClientOpenProduct((int) tempDataRepository.get(PRODUCT_COUNT).getVal()));
        } else if (message.getText().equals(i18nService.getMsg(SAVE_BASKET_BUTTON))) {
            int productCount = (int) tempDataRepository.getAndDelete(PRODUCT_COUNT).getVal();
            UUID productId = (UUID) tempDataRepository.getAndDelete(CURRENT_PRODUCT_ID).getVal();
            Order order = orderService.orderVerify(message.getChatId());
            UUID categoryId = (UUID) tempDataRepository.get(CURRENT_CATEGORY_ID).getVal();
            try {
                orderProductService.createOrderProduct(productId, productCount, order.getId());
                user.setStatus(Status.CHOOSE_PRODUCT);
                authRepository.update(user);
                deletePhotoAndSendMessage(message.getChatId(), message.getMessageId(), CHOOSE_PRODUCT, ButtonService.createClientCategoryProductButton(categoryId));
            } catch (NotEnoughProductException e) {
                GlobalVar.log(Level.SEVERE, e.getMessage(), e);
            }

        }
    }

    public static void openBasket(Update update, Message message) {
        User user = GlobalVar.getUSER();
        if (!update.hasCallbackQuery()) {
            sendMessage(message.getChatId(), INFORMATION_ERROR);
            return;
        }

        Order order = orderService.orderVerify(user.getId());
        if (message.getText().equals(i18nService.getMsg(BACK))) {
            user.setStatus(Status.ORDER_MENU);
            authRepository.update(user);
            deletePhotoAndSendMessage(message.getChatId(), message.getMessageId(), ORDER_MENU, ButtonService.createClientOrderButton());
            return;
        } else if (message.getText().equals(i18nService.getMsg(CONFIRM_ORDER_BUTTON))) {
            Balance balance = null;
            try {
                balance = balanceService.getBalance(GlobalVar.getUSER().getId());
                Long allPrice = (order.getActualPrice() + order.getDeliveryPrice() + order.getServicePrice());
                if (balance.getBalance() < allPrice) {
                    user.setStatus(Status.ADD_MONEY_TO_CARD);
                    authRepository.update(user);
                    sendMessage(message.getChatId(), i18nService.getMsg(NOT_ENOUGH_MONEY).formatted(allPrice - balance.getBalance()));
                    sendMessage(message.getChatId(), ADD_MONEY_TO_CARD, ButtonService.createBackButton());
                    return;
                }
            } catch (BalanceNotFoundException e) {
                user.setStatus(Status.ADD_CARD);
                authRepository.update(user);
                deleteMessage(message.getChatId(), message.getMessageId());
                sendMessage(message.getChatId(), YOU_ARE_NOT_BALANCE, ButtonService.createClientCreateBalance());
                return;
            }

            user.setStatus(Status.CREATE_LOCATION);
            authRepository.update(user);
            deleteMessage(message.getChatId(), message.getMessageId());
            sendMessage(message.getChatId(), CREATE_LOCATION, ButtonService.createShareLocation());
            return;
        } else if (message.getText().equals(i18nService.getMsg(CLEAR_BASKET_BUTTON))) {
            try {
                orderProductService.clearOrderProduct(order.getId());
                order = orderService.orderVerify(user.getId());
            } catch (OrderNotFoundException e) {
                GlobalVar.log(Level.WARNING, e.getMessage());
            }
            user.setStatus(Status.ORDER_MENU);
            authRepository.update(user);
            deleteMessage(message.getChatId(), message.getMessageId());
            sendMessage(message.getChatId(), ORDER_MENU, ButtonService.createClientOrderButton());
            return;
        }


        List<OrderProduct> myAllOrderProduct = orderProductService.getMyAllOrderProduct(order.getId());

        for (OrderProduct orderProduct : myAllOrderProduct) {
            if (orderProduct.getId().toString().equals(message.getText())) {
                try {
                    orderProductService.deleteOrderProduct(orderProduct.getId());
                    order = orderService.orderVerify(user.getId());
                } catch (NotFoundException e) {
                    GlobalVar.log(Level.WARNING, e.getMessage(), e);
                }
                if(order.getActualPrice()<=0){
                    user.setStatus(Status.ORDER_MENU);
                    authRepository.update(user);
                    deleteMessage(message.getChatId(), message.getMessageId());
                    sendMessage(message.getChatId(), ORDER_MENU, ButtonService.createClientOrderButton());
                    return;
                }
                editMessage(message.getChatId(), message.getMessageId(), i18nService.getMsg(OPEN_BASKET).formatted(
                        GlobalVar.getUSER().getFullName(),
                        findOrderProducts(order.getId()),
                        order.getActualPrice(),
                        order.getDeliveryPrice(),
                        order.getServicePrice(),
                        order.getServicePrice() + order.getActualPrice() + order.getDeliveryPrice()
                ), ButtonService.createClientOpenBasket());
                return;
            }
        }


    }

    public static void createMyLocationOpenBasketMenu(Update update, Message message) {
        User user = GlobalVar.getUSER();
        Order order = orderService.orderVerify(user.getId());
        if (message.getText() != null && message.getText().equals(i18nService.getMsg(BACK))) {
            user.setStatus(Status.OPEN_BASKET);
            authRepository.update(user);
            sendMessage(message.getChatId(), i18nService.getMsg(OPEN_BASKET).formatted(
                    GlobalVar.getUSER().getFullName(),
                    findOrderProducts(order.getId()),
                    order.getActualPrice(),
                    order.getDeliveryPrice(),
                    order.getServicePrice(),
                    order.getServicePrice() + order.getActualPrice() + order.getDeliveryPrice()
            ), ButtonService.createClientOpenBasket());
            return;
        }

        if (!message.hasLocation()) {
            sendMessage(message.getChatId(), INFORMATION_ERROR);
            return;
        }

        order.setLocationTo(message.getLocation());
        List<OrderProduct> myAllOrderProduct = orderProductService.getMyAllOrderProduct(order.getId());
        Optional<Product> product;
        Optional<Category> category;
        List<BusinessLocation> locationsOfBusiness;
        for (OrderProduct orderProduct : myAllOrderProduct) {
            product = productRepository.findById(orderProduct.getProductId());
            if (product.isPresent()) {
                category = categoryRepository.findById(product.get().getCategoryID());
                if (category.isEmpty()) continue;
            } else continue;

            try {
                locationsOfBusiness = businessLocationService.getLocationsOfBusiness(category.get().getBusinessId());
            } catch (BusinessNotFoundException e) {
                GlobalVar.log(Level.WARNING, e.getMessage(), e);
                continue;
            }

            boolean anyMatch = locationsOfBusiness.stream()
                    .anyMatch(temp -> order.getLocationsFrom().contains(temp.getLocation()));

            if (anyMatch) {
                continue;
            }
            order.getLocationsFrom().add(locationsOfBusiness.get(0).getLocation());
        }
        orderRepository.update(order);
        try {
            orderService.startOrder(order.getId());
            Order giveOrderToCourier = orderService.giveOrderToCourier(order.getId());
            sendMessageCourier(giveOrderToCourier);
            user.setStatus(Status.ORDER_MENU);
            authRepository.update(user);
            sendMessage(message.getChatId(), ORDER_FINISHED, ButtonService.createClientOrderButton());
        } catch (OrderNotFoundException e) {
            GlobalVar.log(Level.WARNING, e.getMessage(), e);
        } catch (CourierNotFoundException e) {
            sendMessage(user.getId(), WAIT_ORDER, ButtonService.createClientOrderButton());
            user.setStatus(Status.ORDER_MENU);
            authRepository.update(user);
            MyBot myBot = GlobalVar.getMyBot();
            executor.execute(() -> {
                GlobalVar.setMyBot(myBot);
                while (true) {
                    try {
                        Order giveOrderToCourier = orderService.giveOrderToCourier(order.getId());
                        sendMessageCourier(order);
                        sendMessage(giveOrderToCourier.getOwnerId(), ORDER_FINISHED, ButtonService.createClientOrderButton());
                        break;
                    } catch (OrderNotFoundException ex) {
                        GlobalVar.log(Level.WARNING, ex.getMessage(), ex);
                        break;
                    } catch (CourierNotFoundException ignored) {
                    }
                }
            });
        }
    }

    private static void sendMessageCourier(Order order) {
        Optional<User> courierOption = authRepository.findById(order.getCourierId());

        if(courierOption.isEmpty()){
            System.out.println(order);
            return;
        }
        User courier = courierOption.get();


        List<Location> locationsFrom = order.getLocationsFrom();
        Integer locationId;
        for (int i = 0; i < locationsFrom.size(); i++) {
            locationId = sendLocation(courier.getId(), locationsFrom.get(i).getLongitude(), locationsFrom.get(i).getLatitude());
            String productList = findLocationProduct(locationsFrom.get(i), order.getId());
            if (i + 1 == locationsFrom.size())
                sendReplyMessage(courier.getId(), locationId, productList, ButtonService.createCourierPanelButtons(courier.getId()));
            else
                sendReplyMessage(courier.getId(), locationId, productList);
        }
        Location locationTo = order.getLocationTo();
        sendLocation(courier.getId(), locationTo.getLongitude(), locationTo.getLatitude());

    }

    private static String findLocationProduct(Location location, UUID orderId) {
        List<OrderProduct> myAllOrderProduct = orderProductService.getMyAllOrderProduct(orderId);
        StringJoiner res = new StringJoiner(".\n");


        myAllOrderProduct.forEach(new Consumer<OrderProduct>() {
            private int i = 1;

            @Override
            public void accept(OrderProduct temp) {
                Optional<Product> product = productRepository.findById(temp.getProductId());
                if (product.isEmpty()) return;
                Optional<Category> category = categoryRepository.findById(product.get().getCategoryID());
                if (category.isEmpty()) return;
                try {
                    List<BusinessLocation> locationsOfBusiness = businessLocationService.getLocationsOfBusiness(category.get().getBusinessId());
                    if (locationsOfBusiness.stream().anyMatch(tempLocation -> tempLocation.getLocation().equals(location))) {
                        res.add(i++ + ". %s -> %s.".formatted(product.get().getProductName(), temp.getAmount()));
                    }
                } catch (BusinessNotFoundException ignored) {
                }
            }
        });

        return res.toString();
    }

    public static void addCard(Update update, Message message) {
        User user = GlobalVar.getUSER();

        if (i18nService.getMsg(BACK).equals(message.getText())) {
            user.setStatus(Status.OPEN_BASKET);
            authRepository.update(user);
            Order order = orderService.orderVerify(user.getId());
            sendMessage(message.getChatId(), i18nService.getMsg(OPEN_BASKET).formatted(
                    GlobalVar.getUSER().getFullName(),
                    findOrderProducts(order.getId()),
                    order.getActualPrice(),
                    order.getDeliveryPrice(),
                    order.getServicePrice(),
                    order.getServicePrice() + order.getActualPrice() + order.getDeliveryPrice()
            ), ButtonService.createClientOpenBasket());
        } else if (i18nService.getMsg(ADD_CARD_BUTTON).equals(message.getText())) {
            balanceService.createBalance(user.getId());
            user.setStatus(Status.ADD_MONEY_TO_CARD);
            authRepository.update(user);
            sendMessage(message.getChatId(), ADD_MONEY_TO_CARD, ButtonService.createBackButton());
            return;
        } else {
            sendMessage(message.getChatId(), INFORMATION_ERROR);
        }
    }

    public static void addMoneyToCard(Update update, Message message) {
        User user = GlobalVar.getUSER();

        if (!message.hasText()) {
            sendMessage(message.getChatId(), INFORMATION_ERROR);
            return;
        }

        if (message.getText().equals(i18nService.getMsg(BACK))) {
            user.setStatus(Status.OPEN_BASKET);
            authRepository.update(user);
            Order order = orderService.orderVerify(user.getId());
            sendMessage(message.getChatId(), i18nService.getMsg(OPEN_BASKET).formatted(
                    GlobalVar.getUSER().getFullName(),
                    findOrderProducts(order.getId()),
                    order.getActualPrice(),
                    order.getDeliveryPrice(),
                    order.getServicePrice(),
                    order.getServicePrice() + order.getActualPrice() + order.getDeliveryPrice()
            ), ButtonService.createClientOpenBasket());
            return;
        }

        long money = 0L;
        Balance balance = null;
        try {
            money = Long.parseLong(message.getText());
            if(money<10000 || money>1000000){
                sendMessage(message.getChatId(), INFORMATION_ERROR);
                return;
            }
            balance = balanceService.getBalance(user.getId());
            balanceService.addMoney(balance.getCardNumber(), money);
            balance = balanceService.getBalance(user.getId());
        } catch (IllegalArgumentException e) {
            sendMessage(message.getChatId(), INFORMATION_ERROR);
            return;
        } catch (BalanceNotFoundException e) {
            balanceService.createBalance(user.getId());
            addMoneyToCard(update, message);
        } catch (NotFoundException ignored) {
        }

        Order order = orderService.orderVerify(user.getId());

        Long allPrice = order.getActualPrice() + order.getServicePrice() + order.getDeliveryPrice();

        if (balance == null || balance.getBalance() < allPrice) {
            if (balance == null) balance = balanceService.createBalance(user.getId());
            sendMessage(message.getChatId(), i18nService.getMsg(NOT_ENOUGH_MONEY).formatted(allPrice - balance.getBalance()));
            sendMessage(message.getChatId(), ADD_MONEY_TO_CARD, ButtonService.createBackButton());
            return;
        }

        user.setStatus(Status.CREATE_LOCATION);
        authRepository.update(user);
        sendMessage(message.getChatId(), CREATE_LOCATION, ButtonService.createShareLocation());

    }
}
