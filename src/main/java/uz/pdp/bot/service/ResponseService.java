package uz.pdp.bot.service;

import org.telegram.telegrambots.meta.api.methods.send.SendLocation;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageCaption;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageMedia;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.pdp.backend.utils.GlobalVar;
import uz.pdp.backend.utils.MessageKey;

import java.util.logging.Level;

public class ResponseService {
    private static final I18nService i18nService = I18nService.getInstance();
    public static void sendMessage(Long chatId, String text, ReplyKeyboard replyKeyboard){
        try {
            SendMessage sendMessage = SendMessage.builder()
                    .chatId(chatId.toString())
                    .text(text)
                    .replyMarkup(replyKeyboard)
                    .build();
            GlobalVar.getMyBot().execute(sendMessage);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not send message", e);
        }
    }
    public static void sendMessage(Long chatId, MessageKey key, ReplyKeyboard replyKeyboard){
        try {
            SendMessage sendMessage = SendMessage.builder()
                    .chatId(chatId.toString())
                    .text(i18nService.getMsg(key, chatId))
                    .replyMarkup(replyKeyboard)
                    .build();
            GlobalVar.getMyBot().execute(sendMessage);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not send message", e);
        }
    }

    public static void sendMessage(Long chatId, String text){
        try {
            SendMessage sendMessage = SendMessage.builder()
                    .chatId(chatId.toString())
                    .text(text)
                    .build();
            GlobalVar.getMyBot().execute(sendMessage);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not send message", e);
        }
    }
    public static void sendMessage(Long chatId, MessageKey key){
        try {
            SendMessage sendMessage = SendMessage.builder()
                    .chatId(chatId.toString())
                    .text(i18nService.getMsg(key, chatId))
                    .build();
            GlobalVar.getMyBot().execute(sendMessage);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not send message", e);
        }
    }

    public static void sendErrorMessage(Long chatId){
        try {
            SendMessage sendMessage = SendMessage.builder()
                    .chatId(chatId.toString())
                    .text("ERROR")
                    .build();
            GlobalVar.getMyBot().execute(sendMessage);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not send message", e);
        }
    }

    public static void editMessage(Long chatId, Integer messageId, String text){
        try {
            EditMessageText editMessageText = EditMessageText.builder()
                    .chatId(chatId)
                    .messageId(messageId)
                    .text(text)
                    .build();
            GlobalVar.getMyBot().execute(editMessageText);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not edit message", e);
        }
    }

    public static void editMessage(Long chatId, Integer messageId, MessageKey key){
        try {
            EditMessageText editMessageText = EditMessageText.builder()
                    .chatId(chatId)
                    .messageId(messageId)
                    .text(i18nService.getMsg(key, chatId))
                    .build();
            GlobalVar.getMyBot().execute(editMessageText);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not edit message", e);
        }
    }

    public static void editMessage(Long chatId, Integer messageId, String text, ReplyKeyboard inlineReplyMarkup){
        try {
            EditMessageText editMessageText = EditMessageText.builder()
                    .chatId(chatId)
                    .messageId(messageId)
                    .text(text)
                    .replyMarkup((InlineKeyboardMarkup) inlineReplyMarkup)
                    .build();
            GlobalVar.getMyBot().execute(editMessageText);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not edit message", e);
        }
    }

    public static void deleteMessageAndSendPhoto(Long chatId, Integer messageId, String photoURL, MessageKey key, ReplyKeyboard inlineReplyMarkup){
        try {
            deleteMessage(chatId, messageId);
            SendPhoto sendPhoto = SendPhoto.builder()
                    .chatId(chatId)
                    .photo(new InputFile(photoURL))
                    .caption(i18nService.getMsg(key, chatId))
                    .replyMarkup(inlineReplyMarkup)
                    .build();
            GlobalVar.getMyBot().execute(sendPhoto);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not edit message", e);
        }
    }

    public static void deleteMessageAndSendPhoto(Long chatId, Integer messageId, String photoURL, String text, ReplyKeyboard inlineReplyMarkup){
        try {
            deleteMessage(chatId, messageId);
            SendPhoto sendPhoto = SendPhoto.builder()
                    .chatId(chatId)
                    .photo(new InputFile(photoURL))
                    .caption(text)
                    .replyMarkup(inlineReplyMarkup)
                    .build();
            GlobalVar.getMyBot().execute(sendPhoto);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not send photo", e);
        }
    }

    public static void editMessage(Long chatId, Integer messageId, MessageKey key, ReplyKeyboard inlineReplyMarkup){
        try {
            EditMessageText editMessageText = EditMessageText.builder()
                    .chatId(chatId)
                    .messageId(messageId)
                    .text(i18nService.getMsg(key, chatId))
                    .replyMarkup((InlineKeyboardMarkup) inlineReplyMarkup)
                    .build();
            GlobalVar.getMyBot().execute(editMessageText);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not edit message", e);
        } catch (ClassCastException e){
            GlobalVar.log(Level.WARNING, "can not Inline keyboard cast", e);
        }
    }

    public static void deleteMessage(Long chatId, Integer messageId){
        try {
            DeleteMessage deleteMessage = DeleteMessage.builder()
                    .chatId(chatId)
                    .messageId(messageId)
                    .build();
            GlobalVar.getMyBot().execute(deleteMessage);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not delete message", e);
        }
    }

    public static void editDescription(Long chatId, Integer messageId, String caption, ReplyKeyboard inlineReplyMarkup){
        try {
            EditMessageCaption editMessageCaption = EditMessageCaption.builder()
                    .chatId(chatId)
                    .messageId(messageId)
                    .caption(caption)
                    .replyMarkup((InlineKeyboardMarkup) inlineReplyMarkup)
                    .build();
            GlobalVar.getMyBot().execute(editMessageCaption);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not edit message", e);
        }
    }

    public static void deletePhotoAndSendMessage(Long chatId, Integer messageId, MessageKey key, ReplyKeyboard replyKeyboard) {
        try {
            deleteMessage(chatId, messageId);
            SendMessage sendMessage = SendMessage.builder()
                    .chatId(chatId)
                    .text(i18nService.getMsg(key))
                    .replyMarkup(replyKeyboard)
                    .build();
            GlobalVar.getMyBot().execute(sendMessage);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not send message", e);
        }
    }

    public static Integer sendLocation(Long chatId, Double longitude, Double latitude) {
        SendLocation sendLocation = null;
        try {
            sendLocation = SendLocation.builder()
                    .chatId(chatId)
                    .longitude(longitude)
                    .latitude(latitude)
                    .build();
            GlobalVar.getMyBot().execute(sendLocation);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not send location", e);
        }
        return sendLocation.getMessageThreadId();
    }

    public static void sendReplyMessage(Long chatId, Integer replyToMessageId, String text) {
        try {
            SendMessage sendMessage = SendMessage.builder()
                    .chatId(chatId)
                    .text(text)
                    .replyToMessageId(replyToMessageId)
                    .build();
            GlobalVar.getMyBot().execute(sendMessage);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not send location", e);
        }
    }

    public static void sendReplyMessage(Long chatId, Integer replyToMessageId, String text, ReplyKeyboard replyKeyboard) {
        try {
            SendMessage sendMessage = SendMessage.builder()
                    .chatId(chatId)
                    .text(text)
                    .replyToMessageId(replyToMessageId)
                    .replyMarkup(replyKeyboard)
                    .build();
            System.out.println(replyToMessageId);
            System.out.println("sendMessage = " + sendMessage);
            GlobalVar.getMyBot().execute(sendMessage);
        } catch (TelegramApiException e) {
            GlobalVar.log(Level.WARNING, "Can not send location", e);
        }
    }
}
