package uz.pdp.bot.service;

import javassist.NotFoundException;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import uz.pdp.backend.enums.Language;
import uz.pdp.backend.enums.Status;
import uz.pdp.backend.exceptions.BalanceNotFoundException;
import uz.pdp.backend.exceptions.OrderNotFoundException;
import uz.pdp.backend.model.Balance;
import uz.pdp.backend.model.Order;
import uz.pdp.backend.model.User;
import uz.pdp.backend.repository.impl.AuthRepositoryImpl;
import uz.pdp.backend.service.AuthService;
import uz.pdp.backend.service.BalanceService;
import uz.pdp.backend.service.OrderService;
import uz.pdp.backend.utils.GlobalVar;
import uz.pdp.backend.utils.MessageKey;

import java.util.logging.Level;

import static uz.pdp.backend.utils.MessageKey.*;
import static uz.pdp.bot.service.ResponseService.*;

public class CourierMenuService {
    private static final AuthRepositoryImpl authRepository = AuthRepositoryImpl.getInstance();
    private static final AuthService authService = AuthService.getInstance();
    private static final I18nService i18nService = I18nService.getInstance();
    private static final OrderService orderService = OrderService.getInstance();
    private static final BalanceService balanceService = BalanceService.getInstance();

    public static void mainMenu(Update update, Message message) {
        User user = GlobalVar.getUSER();
        if (message.getText().equals(i18nService.getMsg(SETTINGS_BUTTON))) {
            user.setStatus(Status.SETTINGS);
            sendMessage(message.getChatId(), SETTINGS_COURIER, ButtonService.createCourierSettingsButton());
        } else if(message.getText().equals(i18nService.getMsg(ORDER_DELIVERED_BUTTON))) {
            Long id = GlobalVar.getUSER().getId();

            try {
                Order orderOfCourier = orderService.getOrderOfCourier(id);
                orderService.finishOrder(orderOfCourier.getId());
            } catch (NotFoundException e) {
                return;
            } catch (OrderNotFoundException e) {
                GlobalVar.log(Level.WARNING, "Order not found!", e);
            }
            sendMessage(message.getChatId(), ORDER_DELIVERED, ButtonService.createCourierPanelButtons());
        }
        else if (message.getText().equals(i18nService.getMsg(MY_BALANCE_BUTTON))) {
            user.setStatus(Status.MY_BALANCE);
            sendMessage(message.getChatId(), MessageKey.MY_BALANCE, ButtonService.createBusinessmanMyBalance());
        }
        authRepository.update(user);
    }

    public static void settingsMenu(Update update, Message message) {
        User user = GlobalVar.getUSER();
        if (message.getText().equals(i18nService.getMsg(MessageKey.SET_LANGUAGE_BUTTON))) {
            user.setStatus(Status.SET_LANGUAGE);
            sendMessage(message.getChatId(), MessageKey.SET_LANGUAGE, ButtonService.createLanguageButton());
        } else if (message.getText().equals(i18nService.getMsg(MessageKey.BACK))) {
            user.setStatus(Status.MAIN_MENU);
            sendMessage(message.getChatId(), COURIER_PANEL, ButtonService.createCourierPanelButtons());
        } else {
            sendMessage(message.getChatId(), MessageKey.INFORMATION_ERROR);
            return;
        }

        authRepository.update(user);
    }

    public static void setLanguage(Update update, Message message) {
        User user = GlobalVar.getUSER();
        if (!update.hasCallbackQuery()) {
            sendMessage(message.getChatId(), i18nService.getMsg(INFORMATION_ERROR));
            return;
        }
        findLanguage(message.getText(), user);
        user.setStatus(Status.SETTINGS);
        authRepository.update(user);
        editMessage(message.getChatId(), message.getMessageId(), SET_LANGUAGE);
        sendMessage(message.getChatId(), SETTINGS_COURIER, ButtonService.createCourierSettingsButton());
    }
    private static void findLanguage(String text, User user) {
        Language[] values = Language.values();
        for (Language value : values) {
            if (value.getCallbackData().equals(text)) {
                user.setLanguage(value);
                return;
            }
        }
    }

    public static void myBalance(Update update, Message message) {
        User user = GlobalVar.getUSER();
        if (message.getText().equals(i18nService.getMsg(ADD_CARD_BUTTON))){
            try {
                balanceService.getBalance(user.getId());
                sendMessage(message.getChatId(), INFORMATION_ERROR, ButtonService.createCourierMyBalance());
            } catch (BalanceNotFoundException e) {
                balanceService.createBalance(user.getId());
                sendMessage(message.getChatId(), i18nService.getMsg(CARD_CREATED), ButtonService.createCourierMyBalance());
            }
        }
        else if(message.getText().equals(i18nService.getMsg(GET_MY_BALANCE_BUTTON))){
            try {
                Balance balance = balanceService.getBalance(user.getId());
                sendMessage(message.getChatId(), i18nService.getMsg(GET_MY_BALANCE).formatted(
                        balance.getCardNumber(),
                        balance.getBalance()
                ), ButtonService.createCourierMyBalance());
            } catch (BalanceNotFoundException e) {
                sendMessage(message.getChatId(), INFORMATION_ERROR, ButtonService.createCourierMyBalance());
            }
        }
        else if (message.getText().equals(i18nService.getMsg(MessageKey.BACK))) {
            user.setStatus(Status.MAIN_MENU);
            sendMessage(message.getChatId(), COURIER_PANEL, ButtonService.createCourierPanelButtons());
        }
        authRepository.update(user);
    }
}
